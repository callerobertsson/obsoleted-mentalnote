package view

import (
	"../config"
	"../store"
	"bufio"
	"fmt"
	"os"
	"strings"
)

func ListMessages(conf config.Config) {

	messages, err := slack.GetMessages(conf)
	if err != nil {
		fmt.Println("Error: " + err.Error())
		return
	}

	lastDay := ""
	for i := len(messages) - 1; i >= 0; i-- {
		mess := messages[i]
		currDay := mess.GetDateString()

		if lastDay != currDay {
			lastDay = currDay
			fmt.Println("\n" + currDay)
		}
		//text := strings.Replace(mess.Text, "\n", "       \n", -1)
		fmt.Println("  " + mess.GetTimeString() + ": " + mess.Text)
	}
}

func CreateMessageFromCommandLine() string {
	// Skip command name and -m flag
	return strings.Join(os.Args[2:], " ")
}

func ReadMessageFromConsole() string {
	scanner := bufio.NewScanner(os.Stdin)

	var lines []string
	for scanner.Scan() {
		line := scanner.Text()

		if line == "." {
			break
		}

		lines = append(lines, line)
	}

	return strings.TrimSpace(strings.Join(lines, "\n"))
}

func PrintUsageMessage() {
	fmt.Println("USAGE:")
	fmt.Println("\tmn -l             # list mental notes")
	fmt.Println("\tmn -m <MESSAGE>   # save MESSAGE")
	fmt.Println("\tmn                # enter message until EOF or . on empty line")
}
