package config

import (
	"encoding/json"
	"os"
)

type Config struct {
	ApiToken  string
	ChannelId string
	UserName  string
	IconUrl   string
}

func NewConfig(filePath string) (c Config, err error) {

	file, _ := os.Open(filePath)
	decoder := json.NewDecoder(file)

	err = decoder.Decode(&c)

	return c, err
}
