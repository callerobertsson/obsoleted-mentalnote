// mn.go - simple and fast mental note logger

package main

import (
	"fmt"
	"os"
	"os/user"

	"./config"
	"./store"
	"./view"
)

func main() {
	user, err := user.Current()
	if err != nil {
		panic("Could not get current user: " + err.Error())
	}

	confFile := user.HomeDir + "/.mn.json"
	if len(os.Args) > 1 {
		confFile = os.Args[1]
	}

	cfg, err := config.NewConfig(confFile)
	if err != nil {
		panic("Configuration error: " + err.Error())
	}

	message := ""

	if len(os.Args) > 1 && os.Args[1] == "-l" {
		view.ListMessages(cfg)
		return
	}

	if len(os.Args) > 1 && os.Args[1] == "-m" {
		message = view.CreateMessageFromCommandLine()
	} else if len(os.Args) > 1 {
		fmt.Println("Error: Invalid command line")
		view.PrintUsageMessage()
		return
	} else {
		fmt.Println("Enter log. Terminate with ^D")
		message = view.ReadMessageFromConsole()
	}

	if message != "" {
		slack.SendMessage(message, cfg)
		fmt.Println("done!")
	} else {
		fmt.Println("nothing sent!")
	}
}

// EOF
