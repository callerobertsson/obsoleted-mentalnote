package slack

import (
	"../config"
	"../model"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

const (
	ApiBaseUrl = "https://slack.com/api/"
)

func SendMessage(message string, conf config.Config) {

	var u *url.URL
	u, _ = url.Parse(ApiBaseUrl)
	u.Path += "/chat.postMessage"

	params := url.Values{}
	params.Add("token", conf.ApiToken)
	params.Add("channel", conf.ChannelId)
	params.Add("text", message)
	params.Add("username", conf.UserName)
	params.Add("icon_url", conf.IconUrl)

	u.RawQuery = params.Encode()

	client := &http.Client{}
	r, _ := http.NewRequest("GET", u.String(), nil)

	resp, _ := client.Do(r)
	fmt.Println(resp.Status)
}

func GetMessages(conf config.Config) ([]model.Message, error) {

	var u *url.URL
	u, _ = url.Parse(ApiBaseUrl)
	u.Path += "/groups.history"

	params := url.Values{}
	params.Add("token", conf.ApiToken)
	params.Add("channel", conf.ChannelId)

	u.RawQuery = params.Encode()

	client := &http.Client{}
	req, _ := http.NewRequest("GET", u.String(), nil)

	resp, _ := client.Do(req)
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	type MessagesResponse struct {
		Ok       bool            `json:"ok"`
		Error    string          `json:"error"`
		Messages []model.Message `json:"messages"`
	}

	var messageResponse MessagesResponse

	err := json.Unmarshal(body, &messageResponse)

	if err != nil {
		return nil, err
	}

	if messageResponse.Ok {
		return messageResponse.Messages, nil
	}

	return nil, errors.New(messageResponse.Error)
}
