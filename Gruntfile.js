module.exports = function(grunt) {

    grunt.initConfig({
        go: {
            myapp: {
                output: "mn",
                run_files: ["mn.go"]
            }
        },
        watch: {
            scripts: {
                files: ['**/*.go'],
                tasks: ['go:build:myapp']
            }
        }
    });

    grunt.loadNpmTasks('grunt-go');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['watch']);
};

